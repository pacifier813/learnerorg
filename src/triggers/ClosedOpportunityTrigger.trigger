trigger ClosedOpportunityTrigger on Opportunity (before update) {
    
    
    for(Opportunity o:Trigger.New) {
        if(o.StageName == 'Closed Won') {
            
            Attachment a = new Attachment();
            try {
               a = [Select Id, Name from Attachment where ParentId =:o.Id];
            }
            catch(Exception e) {
               a = null;
            }
            
            if (a == null)
               o.addError('Add an attachment before you close the Opportunity');
        }
    }
    

}