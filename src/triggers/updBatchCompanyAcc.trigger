trigger updBatchCompanyAcc on Batch__c (before insert,before update) {

    map<string,account> mapAllAccs = new map<string,account>();
    for(account acc:[select id, name from account]){
        mapAllAccs.put(acc.name,acc);
    }
    
    system.debug('mapAllAccs--->'+mapAllAccs);
    
    for(Batch__c bat: trigger.new){
        system.debug('bat.Company_Name__c--->'+bat.Company_Name__c);
        
        if(mapAllAccs.containsKey(bat.Company_Name__c)){
            system.debug('bat.Company_Name__c--->'+bat.Company_Name__c);
            bat.Account__c=mapAllAccs.get(bat.Company_Name__c).Id;
            system.debug('bat.Account__c--->'+bat.Account__c);
        }
    }
   
}