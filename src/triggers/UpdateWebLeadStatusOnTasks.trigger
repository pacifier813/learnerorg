trigger UpdateWebLeadStatusOnTasks on Task (before insert, before update) {

   List<Web_Lead__c> webleads = new List<Web_Lead__c>();
   Set<Id> leadIds = new Set<Id>();
   Map<Id,Web_Lead__c> leadmap = new Map<Id,Web_Lead__c>();

   for(Task tsk: Trigger.New) {
    
      leadIds.add(tsk.WhatId);
    
   }
   
   for(Web_Lead__c w: [Select Id, Status__c from Web_Lead__c where Id IN:leadIds]) {
       
       leadmap.put(w.Id,w);
       
   }
   
   for(Task t: Trigger.New) {
      
      if(leadmap.containsKey(t.WhatId) && t.subject.contains('Email')) {
         Web_Lead__c wl = leadmap.get(t.WhatId);
         wl.Status__c = 'Contacté';
         webleads.add(wl);
      }   
       
   }
   
   try {
       update webleads;
   }
   catch(Exception e) {
       System.debug(e.getMessage());
   }

}