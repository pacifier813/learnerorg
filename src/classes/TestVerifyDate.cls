@isTest
private class TestVerifyDate {
    @isTest static void testCheckDates() {
        Date d1 = VerifyDate.CheckDates(Date.newInstance(2016,11,1),Date.newInstance(2016,11,20));
        System.assertEquals(Date.newInstance(2016,11,20), d1);
        Date d2 = VerifyDate.CheckDates(Date.newInstance(2016,11,1),Date.newInstance(2016,12,20));
        System.assertEquals(Date.newInstance(2016,11,30), d2);      
    }
}