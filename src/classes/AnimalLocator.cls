public class AnimalLocator {

    public static String getAnimalNameById(Integer idvalue) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        
        String endpoint = 'https://th-apex-http-callout.herokuapp.com/animals/' + String.ValueOf(idvalue);
        
        request.setEndpoint(endpoint);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        
        String animalname;
        // If the request is successful, parse the JSON response.
        if (response.getStatusCode() == 200) {
            // Deserializes the JSON string into collections of primitive data types.
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            // Cast the values in the 'animals' key as a list
            
            Map<String,Object> animalmap = (Map<String,Object>)results.get('animal');
            
            animalname = (String)animalmap.get('name');
            
        }
        return animalname;
    }
        

}