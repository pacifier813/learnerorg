@isTest
private class TestRestrictContactByName {

    @isTest static void TestInvalidContact() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        Contact c = new Contact(LastName='INVALIDNAME');
    
        
        // Perform test
        Test.startTest();
        Database.SaveResult sr = Database.Insert(c, false);
        Test.stopTest();

        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(!sr.isSuccess());
        System.assert(sr.getErrors().size() > 0);
        System.assertEquals('The Last Name "INVALIDNAME" is not allowed for DML',
                             sr.getErrors()[0].getMessage());
    }
    
}