public with sharing class CtrAccountComponent{
 //parameter for the component
 
 public Boolean initialized = false;
 public string testvar {
    get {
    return 'testvalue';
    }
    set;
 }
 public Id accountId {
    get;
    set {
       accountId = value;
       if(!initialized) {
          
          initializeThatDependsOnReceivedString();
          initialized = true;
       
       }
    }
 }
 //object used in component
 public Account theAccount {get;set;}
 
 public void initializeThatDependsOnReceivedString() {
   System.debug('Account ID is'+accountId);
   this.theAccount = [SELECT Id, Name FROM Account WHERE Id=: accountId];
   System.debug('Account sobj'+theAccount);
 
 } 
 
 public void pageactionmethod() {
    System.debug('page method called here');

 }
}