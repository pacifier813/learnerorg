/**********************************************************************************
* File Name   : bbcTest.cls
* Description : Apex test class to perform unit tests onn bbc.cls
* Author      : Akhil Anil
* CreatedDate : 03-06-2018
* *********************************************************************************/ 
@isTest
private class bbcTest {
    @isTest static void testCalloutSuccess() {
        // Use StaticResourceCalloutMock built-in class to
        // specify fake response and include response body 
        // in a static resource.
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('TestSuccess');
        mock.setStatusCode(200);
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        
        // Call the method that performs the callout
        bbc b = new bbc();
        b.fetchdata();
        // Verify response received contains values returned by
        // the mock response.
        // This is the content of the static resource.
        System.assertEquals(10, b.results.size()); 
    }

    @isTest static void testCalloutFailure() {
        // Use StaticResourceCalloutMock built-in class to
        // specify fake response and include response body 
        // in a static resource.
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('TestFailure');
        mock.setStatusCode(200);
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        
        // Call the method that performs the callout
        bbc b = new bbc();
        b.fetchdata();
        // Verify response received contains values returned by
        // the mock response.
        // This is the content of the static resource.
        System.assert(b.results.size() < 10); 
    }    
    
    @isTest static void testCalloutResponseFailure() {
        // Use StaticResourceCalloutMock built-in class to
        // specify fake response and include response body 
        // in a static resource.
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('TestNoResponse');
        mock.setStatusCode(500);
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        
        // Call the method that performs the callout
        bbc b = new bbc();
        b.fetchdata();
        // Verify response received contains values returned by
        // the mock response.
        // This is the content of the static resource.
        System.assert(b.results == null); 
    }     
}