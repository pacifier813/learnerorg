@isTest
public class testBatch {
   
   static testmethod void testinsert() {
      
      integer i = 0;
      
      List<Account> acs = new List<Account>();
      
      for(i=0;i<2;i++) {
         
         String str = 'Test' + String.ValueOf(i);
         acs.add(new Account(Name=str));   
      
      
      }
      
      acs.add(new Account(Name='Test batch'));
      
      try {
         
         insert acs;
      
      }
      catch(Exception e) {
      
      
      }
      
      Batch__c b = new Batch__c();
      b.Name = 'B10';
      b.Company_Name__c = 'Test batch';
      
      try {
         
         insert b;
         
      }
      catch(Exception e) {
      
      }
      
      Batch__c b1 = [Select Id, Name, Company_Name__c from Batch__c where Id =:b.Id];
      
      try {  
        
        update b1;
        
      }
      catch(Exception e) {
      
      } 
      
   }

}