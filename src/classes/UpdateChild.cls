public class UpdateChild {
   
   public List<Account> acs;
   
   public UpdateChild() {
   
      acs = new List<Account>();
      
      System.Debug('Called');
     
   }
   
   public List<Account> getRecord(Account ac, String str) {
      
      List<Account> accts = new List<Account>();
      
      try {
      
         accts = [Select Id, Name, Type, Account__c from Account where Account__c =:ac.Id];
        
      }
      catch(Exception e) {
        
         accts = null;
        
      }  
      
      System.Debug('-----initial----'+accts);
      System.Debug('Size of list'+acs.Size());
             
      if (accts != null) {
         
         for(Account temp:accts) {
            
            temp.Type = str;
            
            acs.add(temp);
            
            System.Debug('-----middle----'+temp);
            
            this.getRecord(temp,str);
         
         }
      
      }
      
      System.Debug('-----final----'+acs);
      
      return acs; 
     
   }

}