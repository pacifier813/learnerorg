public class LightningApexController {
    
    @AuraEnabled
    public static List<Account> fetchAccounts() {
        return [Select Id, Name, Type from Account limit 10];
    }

}