@IsTest
private class AccountManagerTest {

    @isTest static void testGetAccountById() {
        Id recordId = createTestRecord();
        // Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri =
            'https://learning01-dev-ed.my.salesforce.com/services/apexrest/Accounts/'
            + String.ValueOf(recordId) + '/contacts';
            
        request.httpMethod = 'GET';
        RestContext.request = request;
        // Call the method to test
        Account thisa = AccountManager.getAccount();
        // Verify results
        System.assert(thisa != null);
        System.assertEquals('Test Account', thisa.Name);
    }

    // Helper method
    static Id createTestRecord() {
        // Create test record
        Account accTest = new Account(
            Name='Test Account');
        insert accTest;
        Contact c = new Contact(LastName = 'Test Contact', AccountId = accTest.Id);
        insert c;
        return accTest.Id;
    }          

}