/**********************************************************************************
* File Name   : bbc.cls
* Description : Apex class to callout the bbc.com website and fetch the news.
* Author      : Akhil Anil
* CreatedDate : 03-06-2018
* *********************************************************************************/ 

public class bbc {
    
    public List<String> results{get;set;}
    
    public void fetchdata () {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        Endpoints__mdt mdt = [Select DeveloperName, URL__c from Endpoints__mdt where DeveloperName = 'BBC'];
        request.setEndpoint(mdt.URL__c);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        // Parse the JSON response
        if (response.getStatusCode() == 200) {
            System.debug(response.getBody());
            String resp = response.getBody();
            results = new List<String>();
            for(Integer i=1; i < 11; i++) {
                String starttag;
                if (i < 6)
                    starttag = '<li data-entityid="most-popular-read-' + String.ValueOf(i) + '" class="gel-layout__item gs-o-faux-block-link gs-u-mb+ gel-1/2@m gs-u-float-left@m gs-u-clear-left@m gs-u-float-none@xxl">';
                else
                    starttag = '<li data-entityid="most-popular-read-' + String.ValueOf(i) + '" class="gel-layout__item gs-o-faux-block-link gs-u-mb+ gel-1/2@m">';
                String newslink = resp.substringBetween(starttag,'</li>');                    
                if(newslink != null) {
                    newslink = newslink.replace('/news/','http://www.bbc.com/news/');
                    results.add(newslink);
                }    
                else 
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'The response does not contain all top 10 news'));
            }			
        }
        else  {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'A valid response could not be fetched from the website'));
        }        
    }   
}