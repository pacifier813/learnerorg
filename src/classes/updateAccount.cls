global class updateAccount implements Database.Batchable<sObject> {
   
   global string query;
   
   global updateAccount(String q) {
      
      query = q;
      
   }
   
   global Database.QueryLocator start(Database.batchableContext bc) {
      
      return Database.getQueryLocator(query);
   
   }
   
   global void execute(Database.BatchableContext bc,List<sObject> scope) {
      
      List<Account> acts = ((List<Account>)scope);
      for(Account a:acts) {
         
         a.Site = '100';
      
      }
      
      update acts;
   
   }
   
   global void finish(Database.batchableContext bc) {
   
   }

}