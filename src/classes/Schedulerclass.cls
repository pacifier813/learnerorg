public class Schedulerclass {
    
   public static void scheduleresources() {
   
 
     Set<Integer> woks = new Set<Integer>();
     Map<Integer,List<Id>> wmap = new Map<Integer,List<Id>>();  
     Map<Integer,List<Id>> rmap = new Map<Integer,List<Id>>();
   
      List<Work_Order__c> wos =  [Select Id, Name, To_Do_Hrs__c from Work_Order__c order by To_Do_Hrs__c desc];
      for(Work_Order__c w:wos) {
         
         List<Id> ids = new List<Id>();
         if(wmap.containsKey(Integer.ValueOf(w.To_Do_Hrs__c))) {
         
            ids = wmap.get(Integer.ValueOf(w.To_Do_Hrs__c));
            ids.add(w.Id);
         }
         else {
            ids.add(w.Id);
         }
         wmap.put(Integer.ValueOf(w.To_Do_Hrs__c),ids);         
         
         woks.add(Integer.ValueOf(w.To_Do_Hrs__c));
      }
      
      List<Resource__c> resc = [Select Id, Name, Regular_Hrs__c from Resource__c];
      
      for(Resource__c r:resc) {
      
         List<Id> ids = new List<Id>();
         if(rmap.containsKey(Integer.ValueOf(r.Regular_Hrs__c))) {
         
            ids = rmap.get(Integer.ValueOf(r.Regular_Hrs__c));
            ids.add(r.Id);
         }
         else {
            ids.add(r.Id);
         }
         rmap.put(Integer.ValueOf(r.Regular_Hrs__c),ids);             
                
      }
     
         
   }


}