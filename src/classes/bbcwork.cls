public class bbcwork {
   
    public String result{get;set;}   
  public List<String> results{get;set;}
    
    public void fetchdata () {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://www.bbc.com/news');
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        // Parse the JSON response
        if (response.getStatusCode() == 200) {
            System.debug(response.getBody());
      String resp = response.getBody();
      results = new List<String>();
      for(Integer i=1; i < 11; i++) {
           String starttag;
           if (i < 6)
              starttag = '<li data-entityid="most-popular-read-' + String.ValueOf(i) + '" class="gel-layout__item gs-o-faux-block-link gs-u-mb+ gel-1/2@m gs-u-float-left@m gs-u-clear-left@m gs-u-float-none@xxl">';
           else
              starttag = '<li data-entityid="most-popular-read-' + String.ValueOf(i) + '" class="gel-layout__item gs-o-faux-block-link gs-u-mb+ gel-1/2@m">';
           String newslink = resp.substringBetween(starttag,'</li>').replace('/news/','http://www.bbc.com/news/');
               if (newslink != null)
            results.add(newslink);
               else {
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'A valid response could not be fetched for all the top 10 news'));
                  return; 
               }
      }      
        }
        else  {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'A valid response could not be fetched from the website'));
        }        
    }   
}